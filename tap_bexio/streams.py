"""Stream type classes for tap-bexio."""

from pathlib import Path
from typing import Any, Dict, Optional, List, Iterable
import requests
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk import typing as th

from tap_bexio.client import BexioStream, BexioSearchStream


class OrdersStream(BexioSearchStream):
    name = "orders"
    path = "/2.0/kb_order/search"
    primary_keys = ["id"]
    replication_key = "updated_at"

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("document_nr", th.StringType),
        th.Property("title", th.StringType),
        th.Property("contact_id", th.IntegerType),
        th.Property("contact_sub_id", th.IntegerType),
        th.Property("user_id", th.IntegerType),
        th.Property("project_id", th.IntegerType),
        th.Property("logopaper_id", th.IntegerType),
        th.Property("language_id", th.IntegerType),
        th.Property("bank_account_id", th.IntegerType),
        th.Property("currency_id", th.IntegerType),
        th.Property("payment_type_id", th.IntegerType),
        th.Property("header", th.StringType),
        th.Property("footer", th.StringType),
        th.Property("total_gross", th.StringType),
        th.Property("total_net", th.StringType),
        th.Property("total_taxes", th.StringType),
        th.Property("total", th.StringType),
        th.Property("total_rounding_difference", th.NumberType),
        th.Property("mwst_type", th.IntegerType),
        th.Property("mwst_is_net", th.BooleanType),
        th.Property("show_position_taxes", th.BooleanType),
        th.Property("is_valid_from", th.DateTimeType),
        th.Property("contact_address", th.StringType),
        th.Property("delivery_address_type", th.IntegerType),
        th.Property("delivery_address", th.StringType),
        th.Property("kb_item_status_id", th.IntegerType),
        th.Property("is_recurring", th.BooleanType),
        th.Property("api_reference", th.StringType),
        th.Property("viewed_by_client_at", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("template_slug", th.StringType),
        th.Property(
            "taxes",
            th.ArrayType(
                th.ObjectType(
                    th.Property("percentage", th.StringType),
                    th.Property("value", th.StringType),
                )
            ),
        ),
        th.Property("network_link", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "order_id": record["id"],
        }


class InvoiceStream(BexioSearchStream):
    name = "invoice"
    path = "/2.0/kb_invoice/search"
    primary_keys = ["id"]
    replication_key = "updated_at"

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("document_nr", th.StringType),
        th.Property("title", th.StringType),
        th.Property("contact_id", th.IntegerType),
        th.Property("contact_sub_id", th.IntegerType),
        th.Property("user_id", th.IntegerType),
        th.Property("project_id", th.IntegerType),
        th.Property("logopaper_id", th.IntegerType),
        th.Property("language_id", th.IntegerType),
        th.Property("bank_account_id", th.IntegerType),
        th.Property("currency_id", th.NumberType),
        th.Property("payment_type_id", th.NumberType),
        th.Property("header", th.StringType),
        th.Property("footer", th.StringType),
        th.Property("total_gross", th.StringType),
        th.Property("total_net", th.StringType),
        th.Property("total_taxes", th.StringType),
        th.Property("total_received_payments", th.StringType),
        th.Property("total_credit_vouchers", th.StringType),
        th.Property("total_remaining_payments", th.StringType),
        th.Property("total", th.StringType),
        th.Property("total_rounding_difference", th.NumberType),
        th.Property("mwst_type", th.IntegerType),
        th.Property("mwst_is_net", th.BooleanType),
        th.Property("show_position_taxes", th.BooleanType),
        th.Property("is_valid_from", th.DateTimeType),
        th.Property("is_valid_to", th.DateTimeType),
        th.Property("contact_address", th.StringType),
        th.Property("kb_item_status_id", th.IntegerType),
        th.Property("reference", th.StringType),
        th.Property("api_reference", th.StringType),
        th.Property("viewed_by_client_at", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("esr_id", th.NumberType),
        th.Property("qr_invoice_id", th.NumberType),
        th.Property("template_slug", th.StringType),
        th.Property(
            "taxes",
            th.ArrayType(
                th.ObjectType(
                    th.Property("percentage", th.StringType),
                    th.Property("value", th.StringType),
                )
            ),
        ),
        th.Property("network_link", th.StringType),
    ).to_dict()


class DeliveriesStream(BexioStream):
    name = "deliveries"
    path = "/2.0/kb_delivery"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("document_nr", th.StringType),
        th.Property("title", th.StringType),
        th.Property("contact_id", th.IntegerType),
        th.Property("contact_sub_id", th.IntegerType),
        th.Property("user_id", th.IntegerType),
        th.Property("logopaper_id", th.IntegerType),
        th.Property("language_id", th.IntegerType),
        th.Property("bank_account_id", th.IntegerType),
        th.Property("currency_id", th.IntegerType),
        th.Property("header", th.StringType),
        th.Property("footer", th.StringType),
        th.Property("total_gross", th.StringType),
        th.Property("total_net", th.StringType),
        th.Property("total_taxes", th.StringType),
        th.Property("total", th.StringType),
        th.Property("total_rounding_difference", th.NumberType),
        th.Property("mwst_type", th.IntegerType),
        th.Property("mwst_is_net", th.BooleanType),
        th.Property("is_valid_from", th.DateTimeType),
        th.Property("contact_address", th.StringType),
        th.Property("delivery_address_type", th.IntegerType),
        th.Property("delivery_address", th.StringType),
        th.Property("kb_item_status_id", th.StringType),
        th.Property("api_reference", th.StringType),
        th.Property("viewed_by_client_at", th.StringType),
        th.Property("updated_at", th.DateTimeType),
    ).to_dict()


class PurchaseOrdersStream(BexioStream):
    name = "purchase_orders"
    path = "/3.0/purchase_orders"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("document_nr", th.StringType),
        th.Property("kb_payment_template_id", th.IntegerType),
        th.Property("payment_type_id", th.IntegerType),
        th.Property("title", th.StringType),
        th.Property("contact_id", th.IntegerType),
        th.Property("contact_sub_id", th.IntegerType),
        th.Property("template_slug", th.StringType),
        th.Property("user_id", th.IntegerType),
        th.Property("project_id", th.IntegerType),
        th.Property("logopaper_id", th.IntegerType),
        th.Property(
            "language",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("name", th.StringType),
                th.Property("decimalpoint", th.StringType),
                th.Property("thousandsseparator", th.StringType),
                th.Property("iso_639_1", th.StringType),
                th.Property("date_format", th.StringType),
            ),
        ),
        th.Property("language_id", th.IntegerType),
        th.Property("bank_account_id", th.IntegerType),
        th.Property(
            "currency",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("name", th.StringType),
                th.Property("round_factor", th.NumberType),
            ),
        ),
        th.Property("currency_id", th.IntegerType),
        th.Property("header", th.StringType),
        th.Property("footer", th.StringType),
        th.Property("total_rounding_difference", th.NumberType),
        th.Property("mwst_type", th.StringType),
        th.Property("mwst_is_net", th.BooleanType),
        th.Property("is_compact_view", th.BooleanType),
        th.Property("show_position_taxes", th.BooleanType),
        th.Property("salesman_user_id", th.IntegerType),
        th.Property("is_valid_from", th.DateTimeType),
        th.Property("is_valid_to", th.DateTimeType),
        th.Property("delivery_address_type", th.StringType),
        th.Property("contact_address_manual", th.StringType),
        th.Property("delivery_address_manual", th.StringType),
        th.Property("nb_decimals_amount", th.IntegerType),
        th.Property("nb_decimals_price", th.IntegerType),
        th.Property("kb_item_status_id", th.IntegerType),
        th.Property("terms_of_payment_text", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("api_reference", th.StringType),
        th.Property("mail", th.StringType),
        th.Property("viewed_by_client_at", th.DateTimeType),
        th.Property("is_valid_until", th.DateTimeType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property(
            "custom_translations",
            th.CustomType({"type": ["array", "object", "string"]}),
        ),
        th.Property("date_format", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "purchase_order_id": record["id"],
        }


class CurrenciesStream(BexioStream):
    name = "currencies"
    path = "/3.0/currencies"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("round_factor", th.NumberType),
    ).to_dict()


class ContactsStream(BexioStream):
    name = "contacts"
    path = "/2.0/contact"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("nr", th.StringType),
        th.Property("contact_type_id", th.IntegerType),
        th.Property("name_1", th.StringType),
        th.Property("name_2", th.StringType),
        th.Property("salutation_id", th.IntegerType),
        th.Property("salutation_form", th.IntegerType),
        th.Property("title_id", th.IntegerType),
        th.Property("birthday", th.DateTimeType),
        th.Property("address", th.StringType),
        th.Property("postcode", th.StringType),
        th.Property("city", th.StringType),
        th.Property("country_id", th.IntegerType),
        th.Property("mail", th.StringType),
        th.Property("mail_second", th.StringType),
        th.Property("phone_fixed", th.StringType),
        th.Property("phone_fixed_second", th.StringType),
        th.Property("phone_mobile", th.StringType),
        th.Property("fax", th.StringType),
        th.Property("url", th.StringType),
        th.Property("skype_name", th.StringType),
        th.Property("remarks", th.StringType),
        th.Property("language_id", th.IntegerType),
        th.Property("is_lead", th.BooleanType),
        th.Property("contact_group_ids", th.StringType),
        th.Property("contact_branch_ids", th.StringType),
        th.Property("user_id", th.IntegerType),
        th.Property("owner_id", th.IntegerType),
        th.Property("updated_at", th.DateTimeType),
    ).to_dict()


class ItemsStream(BexioStream):
    name = "items"
    path = "/2.0/article"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("user_id", th.IntegerType),
        th.Property("article_type_id", th.IntegerType),
        th.Property("contact_id", th.IntegerType),
        th.Property("deliverer_code", th.StringType),
        th.Property("deliverer_name", th.StringType),
        th.Property("deliverer_description", th.StringType),
        th.Property("intern_code", th.StringType),
        th.Property("intern_name", th.StringType),
        th.Property("intern_description", th.StringType),
        th.Property("purchase_price", th.StringType),
        th.Property("sale_price", th.StringType),
        th.Property("purchase_total", th.StringType),
        th.Property("sale_total", th.StringType),
        th.Property("currency_id", th.IntegerType),
        th.Property("tax_income_id", th.IntegerType),
        th.Property("tax_id", th.IntegerType),
        th.Property("tax_expense_id", th.IntegerType),
        th.Property("unit_id", th.IntegerType),
        th.Property("is_stock", th.BooleanType),
        th.Property("stock_id", th.IntegerType),
        th.Property("stock_place_id", th.IntegerType),
        th.Property("stock_nr", th.NumberType),
        th.Property("stock_min_nr", th.NumberType),
        th.Property("stock_reserved_nr", th.NumberType),
        th.Property("stock_available_nr", th.NumberType),
        th.Property("stock_picked_nr", th.NumberType),
        th.Property("stock_disposed_nr", th.NumberType),
        th.Property("stock_ordered_nr", th.NumberType),
        th.Property("width", th.IntegerType),
        th.Property("height", th.IntegerType),
        th.Property("weight", th.IntegerType),
        th.Property("volume", th.IntegerType),
        th.Property("html_text", th.StringType),
        th.Property("remarks", th.StringType),
        th.Property("delivery_price", th.StringType),
        th.Property("article_group_id", th.IntegerType),
    ).to_dict()


class StockLocationsStream(BexioStream):
    name = "stock_locations"
    path = "/2.0/stock"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType), th.Property("name", th.StringType)
    ).to_dict()


class ItemPositionsStream(BexioStream):
    name = "item_positions"
    path = "/2.0/kb_order/{order_id}/kb_position_article"
    parent_stream_type = OrdersStream

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("order_id", th.IntegerType),
        th.Property("type", th.StringType),
        th.Property("amount", th.StringType),
        th.Property("unit_id", th.IntegerType),
        th.Property("account_id", th.IntegerType),
        th.Property("unit_name", th.StringType),
        th.Property("tax_id", th.NumberType),
        th.Property("tax_value", th.StringType),
        th.Property("text", th.StringType),
        th.Property("unit_price", th.StringType),
        th.Property("discount_in_percent", th.StringType),
        th.Property("position_total", th.StringType),
        th.Property("pos", th.StringType),
        th.Property("internal_pos", th.IntegerType),
        th.Property("parent_id", th.IntegerType),
        th.Property("is_optional", th.BooleanType),
        th.Property("article_id", th.IntegerType),
    ).to_dict()

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        response_data = response.json()
        order_id = int(response.url.split("/")[-2])
        for item in response_data:
            item["order_id"] = order_id

        yield from extract_jsonpath(self.records_jsonpath, input=response_data)


class PurchaseOrderDetailsStream(BexioStream):
    name = "purchase_orders_details"
    path = "/3.0/purchase_orders/{purchase_order_id}"
    primary_keys = ["id"]
    parent_stream_type = PurchaseOrdersStream
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("document_nr", th.StringType),
        th.Property("kb_payment_template_id", th.IntegerType),
        th.Property("payment_type_id", th.IntegerType),
        th.Property("title", th.StringType),
        th.Property("contact_id", th.IntegerType),
        th.Property("contact_sub_id", th.IntegerType),
        th.Property("template_slug", th.StringType),
        th.Property("user_id", th.IntegerType),
        th.Property("project_id", th.IntegerType),
        th.Property("logopaper_id", th.IntegerType),
        th.Property(
            "language",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("name", th.StringType),
                th.Property("decimalpoint", th.StringType),
                th.Property("thousandsseparator", th.StringType),
                th.Property("iso_639_1", th.StringType),
                th.Property("date_format", th.StringType),
            ),
        ),
        th.Property("language_id", th.IntegerType),
        th.Property("bank_account_id", th.IntegerType),
        th.Property(
            "currency",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("name", th.StringType),
                th.Property("round_factor", th.NumberType),
            ),
        ),
        th.Property("currency_id", th.IntegerType),
        th.Property("header", th.StringType),
        th.Property("footer", th.StringType),
        th.Property("total_rounding_difference", th.NumberType),
        th.Property("mwst_type", th.StringType),
        th.Property("mwst_is_net", th.BooleanType),
        th.Property("is_compact_view", th.BooleanType),
        th.Property("show_position_taxes", th.BooleanType),
        th.Property("salesman_user_id", th.IntegerType),
        th.Property("is_valid_from", th.DateTimeType),
        th.Property("is_valid_to", th.DateTimeType),
        th.Property("delivery_address_type", th.StringType),
        th.Property("contact_address_manual", th.StringType),
        th.Property("delivery_address_manual", th.StringType),
        th.Property("nb_decimals_amount", th.IntegerType),
        th.Property("nb_decimals_price", th.IntegerType),
        th.Property("kb_item_status_id", th.IntegerType),
        th.Property("terms_of_payment_text", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("api_reference", th.StringType),
        th.Property("mail", th.StringType),
        th.Property("viewed_by_client_at", th.DateTimeType),
        th.Property("is_valid_until", th.DateTimeType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property(
            "custom_translations",
            th.CustomType({"type": ["array", "object", "string"]}),
        ),
        th.Property("date_format", th.CustomType({"type": ["object", "string"]})),
        th.Property("positions", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()

class CountriesStream(BexioStream):
    name = "countries"
    path = "/2.0/country"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("name_short", th.StringType),
        th.Property("iso3166_alpha2", th.StringType),
    ).to_dict()
class TaxesStream(BexioStream):
    name = "taxes"
    path = "/3.0/taxes"
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("uuid", th.StringType),
        th.Property("name", th.StringType),
        th.Property("code", th.StringType),
        th.Property("digit", th.StringType),
        th.Property("type", th.StringType),
        th.Property("account_id", th.NumberType),
        th.Property("tax_settlement_type", th.StringType),
        th.Property("value", th.NumberType),
        th.Property("net_tax_value", th.CustomType({"type": ["number", "string"]})),
        th.Property("start_year", th.NumberType),
        th.Property("end_year", th.NumberType),
        th.Property("is_active", th.BooleanType),
        th.Property("display_name", th.StringType),

    ).to_dict()
