"""REST client handling, including BexioStream base class."""
import json
from datetime import datetime
import requests
from pathlib import Path
from typing import Any, Dict, Optional, Callable
import backoff
import logging

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BearerTokenAuthenticator
from singer_sdk.exceptions import RetriableAPIError
from tap_bexio.auth import OAuth2Authenticator
from pendulum import parse
import time


# logging.getLogger("backoff").setLevel(logging.CRITICAL)


class BexioStream(RESTStream):
    """Bexio stream class."""

    url_base = "https://api.bexio.com"
    records_jsonpath = "$[*]"
    _page_size = 500

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        return OAuth2Authenticator(
            self, self._tap.config_file, "https://idp.bexio.com/token"
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        headers["Accept"] = "application/json"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
        else:
            results = response.json()
            query = requests.utils.urlparse(response.url).query
            params = dict(x.split("=") for x in query.split("&"))
            return (
                int(params["offset"]) + self._page_size
                if len(results) >= self._page_size
                else None
            )

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["limit"] = self._page_size
        params["offset"] = next_page_token or 0
        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key
        return params

    def request_decorator(self, func: Callable) -> Callable:
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
            ),
            max_tries=8,
            factor=3,
        )(func)
        return decorator

    def validate_response(self, response: requests.Response) -> None:
        if 400 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
                f"{response.reason} for url: {response.url}"
                f"{response.reason} Headers: {response.headers}"
            )
            if response.status_code == 429:
                headers = dict(response.headers)
                if "RateLimit-Reset" in headers:
                    time.sleep(int(headers['RateLimit-Reset']))

            raise RetriableAPIError(msg)


    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date


class BexioSearchStream(BexioStream):
    rest_method = "post"

    def prepare_request_payload(self, context, next_page_token):
        return [
            {
                "field": self.replication_key,
                "value": self.get_starting_time(context).isoformat().replace("T", " ").split("+")[0],
                "criteria": ">=",
            }
        ]