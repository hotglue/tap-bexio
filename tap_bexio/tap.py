"""Bexio tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_bexio.streams import (
    ContactsStream,
    OrdersStream,
    ItemsStream,
    CurrenciesStream,
    PurchaseOrdersStream,
    InvoiceStream,
    StockLocationsStream,
    ItemPositionsStream,
    PurchaseOrderDetailsStream,
    CountriesStream,
    TaxesStream,
)

STREAM_TYPES = [
    ContactsStream,
    OrdersStream,
    ItemsStream,
    PurchaseOrdersStream,
    InvoiceStream,
    CurrenciesStream,
    StockLocationsStream,
    ItemPositionsStream,
    PurchaseOrderDetailsStream,
    CountriesStream,
    TaxesStream,
]


class TapBexio(Tap):
    """Bexio tap class."""

    name = "tap-bexio"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        super().__init__(config, catalog, state, parse_env_config, validate_config)
        self.config_file = config[0]

    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapBexio.cli()
